const STATUS_CALL = {
  BUSY: 0, // máy bận
  SUCCESS: 1, // cuộc gọi thành công
  CANCEL: 2, // hủy cuộc gọi
  NO_ANSWER: 3, // khách hàng ko nghe máy
  UNAVAILABLE: 4, // só không đúng hoặc không gọi được
};

export { STATUS_CALL };
