import axios from 'axios';

const url = process.env.SERVER_URL || 'http://localhost:5000';
console.log(url);

const axiosClient = axios.create({
  baseURL: url,
  headers: {
    'content-type': 'application/json',
    'Access-Control-Allow-Origin': '*',
  },
});

export default axiosClient;
