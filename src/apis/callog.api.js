import api from '../configs/axios.config';

const callogApi = {
  createACallog: async (data, config = {}) => {
    const url = `/callog`;
    const response = await api.post(url, data, config);
    return response;
  },

  getNCallog: async () => {
    const url = `/callog`;
    const response = await api.get(url);
    return response && response.data.success && response.data.data;
  },
};

export default callogApi;
