import { configureStore } from '@reduxjs/toolkit';
import sip from '../components/Modal/modalSlice';

export const store = configureStore({
  reducer: {
    sip: sip,
  },
});
