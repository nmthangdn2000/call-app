import JsSIP from 'jssip';
import callogApi from '../apis/callog.api';
import { STATUS_CALL } from '../common/constants';

let coolPhone;
let session;
let recorder = null;
let phoneNumber = null;

const setUpJsSip = (sip) => {
  const { sipUri, webSocket, password } = sip;
  const socket = new JsSIP.WebSocketInterface(webSocket);
  let configuration = {
    sockets: [socket],
    uri: sipUri,
    password: password,
  };

  coolPhone = new JsSIP.UA(configuration);

  coolPhone.start();

  coolPhone.on('registered', function (e) {
    console.log('registered');
  });
  coolPhone.on('unregistered', function (e) {
    console.log('unregistered');
  });
  coolPhone.on('registrationFailed', function (e) {
    console.log('registrationFailed');
  });
  coolPhone.on('connected', function (e) {
    console.log('connected');
  });

  coolPhone.on('disconnected', function (e) {
    console.log('disconnected');
  });
};

const makingOutCall = (phone) => {
  phoneNumber = phone;
  const options = {
    mediaConstraints: { audio: true, video: false },
    sessionTimersExpires: 180,
  };

  coolPhone.call(phone, options);
};

const newSession = (enCall, setSession) => {
  coolPhone.on('newRTCSession', function (ev) {
    const newSession = ev.session;
    session = newSession;

    console.log('newRTCSession ', newSession);
    session.on('ended', () => {
      console.log('ended');
      enCall();
      if (recorder) uploadRecorder();
    });
    session.on('failed', (e) => {
      console.log('failed', e.cause);
      enCall();
      uploadCallog(e.cause);
    });
    session.on('accepted', () => console.log('accepted'));
    session.connection.addEventListener('addstream', function (e) {
      // set remote audio stream
      const remoteAudio = document.createElement('audio');
      remoteAudio.srcObject = e.stream;
      remoteAudio.play();

      recorder = new MediaRecorder(e.stream);
      recorder.start();
    });

    setSession(session);
  });
};

const uploadRecorder = () => {
  const items = [];
  recorder.stop();
  recorder.ondataavailable = (e) => {
    items.push(e.data);
    if (recorder.state === 'inactive') {
      const blob = new Blob(items, { type: 'audio/wav; codecs=0' });
      postData(blob);
    }
  };
};
const postData = (blob) => {
  console.log('uploading...');

  const data = new FormData();

  data.append('phone', phoneNumber);
  data.append('status', STATUS_CALL.SUCCESS);
  data.append('audio', blob, 'recording.wav');

  const config = {
    headers: { 'content-type': 'multipart/form-data' },
  };

  callogApi.createACallog(data, config);
};

const uploadCallog = (type) => {
  let data = { phone: phoneNumber };
  switch (type) {
    case JsSIP.C.causes.BUSY:
      data = { ...data, status: STATUS_CALL.BUSY };
      break;
    case JsSIP.C.causes.CANCELED:
      data = { ...data, status: STATUS_CALL.CANCEL };
      break;
    case JsSIP.C.causes.UNAVAILABLE:
      data = { ...data, status: STATUS_CALL.UNAVAILABLE };
      break;
    default:
      break;
  }
  console.log(data);
  return callogApi.createACallog(data);
};

export { setUpJsSip, makingOutCall, newSession };
