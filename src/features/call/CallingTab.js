import React, { useEffect, useState } from 'react';
import * as AiIcons from 'react-icons/ai';
import * as BsIcons from 'react-icons/bs';
import { makingOutCall, newSession } from '../../util/jsSip';

function CallingTab(props) {
  const [session, setSession] = useState(null);
  const [mic, setMic] = useState(true);
  const [pause, setPause] = useState(true);
  const [showKeyBoard, setShowKeyBoard] = useState(true);
  const { setPhoneNumber, hanldeButtonNumber, phoneNumber } = props;
  const keyBoard = [1, 2, 3, 4, 5, 6, 7, 8, 9, '*', 0, '#'];

  const endCall = () => {
    if (session) {
      session.terminate();
    }
    setShowKeyBoard(true);
  };
  const calling = () => {
    makingOutCall(phoneNumber);
    setShowKeyBoard(false);
  };
  const removeNumber = () => {
    const phone = phoneNumber.split('');
    phone.splice(phoneNumber.length - 1, 1);
    return setPhoneNumber(phone.join(''));
  };
  const handleClickMic = () => {
    setMic(!mic);
  };
  const handleClickPause = () => {
    setPause(!pause);
  };
  const btnAction = [
    {
      icon: mic ? <BsIcons.BsMicFill /> : <BsIcons.BsMicMuteFill />,
      cName: 'btn-action',
      onClick: handleClickMic,
    },
    {
      icon: pause ? <BsIcons.BsFillPauseFill /> : <BsIcons.BsFillPlayFill />,
      cName: 'btn-action',
      onClick: handleClickPause,
    },
  ];

  useEffect(() => {
    newSession(endCall, setSession);
  }, []);

  return (
    <div className="tab1">
      <div className={showKeyBoard ? 'keyboard active' : 'keyboard'}>
        <div className="phone_number">
          <input
            type="text"
            name="phone_number"
            id="phone_number"
            placeholder="Phone Number"
            value={phoneNumber}
            onChange={(e) => setPhoneNumber(e.target.value)}
          />
          <AiIcons.AiOutlineCloseCircle className="btn-remove" onClick={removeNumber} />
        </div>
        <div className="number">
          {keyBoard.map((item, index) => {
            return (
              <div key={index}>
                <span onClick={() => hanldeButtonNumber(item)}>{item}</span>
              </div>
            );
          })}
        </div>
        <div className="btn-calling" onClick={calling}>
          <span>
            <BsIcons.BsFillTelephoneFill />
          </span>
        </div>
      </div>
      <div className={!showKeyBoard ? 'calling active' : 'calling'}>
        <h4>Nguyễn Minh Thắng</h4>
        <h5>0905646804</h5>
        <span>Đang gọi ...</span>
        <div className="calling-action">
          {btnAction.map((item, index) => {
            return (
              <div key={index} className={item.cName} onClick={item.onClick}>
                <span>{item.icon}</span>
              </div>
            );
          })}
        </div>
        <div className="btn-calling end-call" onClick={endCall}>
          <span>
            <BsIcons.BsFillTelephoneFill />
          </span>
        </div>
      </div>
    </div>
  );
}

export default CallingTab;
