import React, { useState } from 'react';
import './Call.css';
import { Row } from 'reactstrap';
import CallTab from './CallTab';
import CallingTab from './CallingTab';
import HistoriesTab from './HistoriesTab';

function Call() {
  const [phoneNumber, setPhoneNumber] = useState('');
  const [currentTab, setCurrentTab] = useState(true);
  const hanldeButtonNumber = (number) => {
    setPhoneNumber(`${phoneNumber}${number}`);
  };

  return (
    <>
      <div className="call-main">
        <div>
          <Row className="call-right">
            <CallTab setCurrentTab={setCurrentTab} />
          </Row>
          <Row style={{ height: '100%' }}>
            <div style={{ display: currentTab ? 'block' : 'none' }}>
              <CallingTab
                setPhoneNumber={setPhoneNumber}
                hanldeButtonNumber={hanldeButtonNumber}
                phoneNumber={phoneNumber}
              />
            </div>
            <div style={{ display: !currentTab ? 'block' : 'none' }}>
              <HistoriesTab currentTab={currentTab} />
            </div>
          </Row>
        </div>
      </div>
    </>
  );
}

export default Call;
