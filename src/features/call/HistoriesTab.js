import React, { useEffect, useState } from 'react';
import { Col, Row } from 'reactstrap';
import callogApi from '../../apis/callog.api';
import { STATUS_CALL } from '../../common/constants';

const url = 'http://localhost:5000';

function HistoriesTab({ currentTab }) {
  const [dataCall, setDataCall] = useState([]);
  useEffect(() => {
    const getData = async () => {
      const data = await callogApi.getNCallog();
      setDataCall(data);
    };
    getData();
  }, [currentTab]);

  const getStatus = (value) => {
    return Object.keys(STATUS_CALL).find((key) => STATUS_CALL[key] === value);
  };

  const showAudio = (item) => {
    if (!item.audio) return <></>;
    return (
      <audio controls>
        <source src={`${url}/uploads/${item.audio}`} type="audio/wav"></source>
      </audio>
    );
  };
  return (
    <>
      {dataCall.map((item, index) => {
        return (
          <div key={index} className="item-his">
            <Row>
              <Row>
                <Col xs="8">
                  <span>
                    <b>Bạn</b> đã gọi cho <b>{item.phone}</b>
                  </span>
                  <br />
                  <span>{new Date(item.createdAt).toLocaleDateString()}</span>
                </Col>
                <Col xs="4">
                  <span>Status </span>
                  <span className="his-status">
                    <b>{getStatus(item.status)}</b>
                  </span>
                </Col>
              </Row>
              {showAudio(item)}
            </Row>
          </div>
        );
      })}
    </>
  );
}

export default HistoriesTab;
