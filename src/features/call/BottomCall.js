import React, { useEffect, useState } from 'react';
import * as BsIcons from 'react-icons/bs';

function BottomCall(props) {
  const { iconMic, setIconMic, iconCamera, setIconCamera } = props;
  //   const [iconPhone, setIconPhone] = useState(true);
  const [mediaStream, setMediaStream] = useState(null);
  const startVideo = () => {
    navigator.getUserMedia(
      { audio: true, video: false },
      (stream) => {
        const video = document.getElementsByClassName('app__videoFeed')[0];
        if (video) {
          video.srcObject = stream;
          setMediaStream(stream);
        }
      },
      (err) => console.error(err)
    );
  };

  const stopVideo = () => {
    console.log(mediaStream.getTracks()[0]);
    mediaStream.getTracks().forEach(function (track) {
      track.stop();
    });
  };

  const handleClickIconMic = () => {
    console.log(mediaStream.getAudioTracks()[0]);
    mediaStream.getAudioTracks()[0].enabled = !iconMic;
    setIconMic(!iconMic);
  };
  const handleClickIconPhone = () => {};
  const handleClickIconCamera = () => {
    console.log(iconCamera);
    if (iconCamera) stopVideo();
    else startVideo();
    setIconCamera(!iconCamera);
  };

  const btnAction = [
    {
      icon: iconMic ? <BsIcons.BsMicFill /> : <BsIcons.BsMicMuteFill />,
      cName: 'btn-action',
      onClick: handleClickIconMic,
    },
    {
      icon: <BsIcons.BsFillTelephoneFill />,
      cName: 'btn-action',
      onClick: handleClickIconPhone,
    },
    {
      icon: iconCamera ? <BsIcons.BsCameraVideoFill /> : <BsIcons.BsCameraVideoOffFill />,
      cName: 'btn-action',
      onClick: handleClickIconCamera,
    },
  ];

  useEffect(() => {
    return startVideo();
  }, []);

  return (
    <div>
      {btnAction.map((btn, index) => {
        return (
          <div key={index} className={btn.cName} onClick={btn.onClick}>
            {btn.icon}
          </div>
        );
      })}
    </div>
  );
}

export default BottomCall;
