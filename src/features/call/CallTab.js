import React, { useState } from 'react';

function CallTab(props) {
  const { setCurrentTab } = props;
  const [showTab, setShowtab] = useState(true);
  const handleClickTab = (type) => {
    setCurrentTab(type);
    setShowtab(type);
  };
  return (
    <>
      <div className="tab">
        <div className={showTab ? 'active' : ''} onClick={() => handleClickTab(true)}>
          <span>Calling</span>
        </div>
        <div className={!showTab ? 'active' : ''} onClick={() => handleClickTab(false)}>
          <span>Histories</span>
        </div>
      </div>
    </>
  );
}

export default CallTab;
