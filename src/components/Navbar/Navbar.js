import React, { useState } from 'react';
import ModelSetting from '../Modal/ModelSetting';
import * as BsIcons from 'react-icons/bs';
import * as AiIcons from 'react-icons/ai';
import { IconContext } from 'react-icons';
import './Navbar.css';
import { Col, Row, Container } from 'reactstrap';

function Navbar() {
  const [modal, setModal] = useState(false);
  const toggleModel = () => setModal(!modal);

  return (
    <>
      <IconContext.Provider value={{ color: '#10b665' }}>
        <div className="navbar">
          <Container>
            <ModelSetting modal={modal} toggleModel={toggleModel} />
            <Row style={{ width: '100%' }}>
              <Col className="btn-phone">
                <div onClick={toggleModel}>
                  <div className="icon">
                    <AiIcons.AiOutlineSetting color="#fff" />
                  </div>
                  <span>Setting</span>
                </div>
              </Col>
              <Col className="btn-phone btn-phone-right">
                <div>
                  <div className="icon">
                    <BsIcons.BsFillTelephoneFill color="#fff" />
                  </div>
                  <span>Phonebook</span>
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </IconContext.Provider>
    </>
  );
}

export default Navbar;
