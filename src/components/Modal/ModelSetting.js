import React, { useEffect, useState } from 'react';
import { Modal, ModalBody, ModalHeader, ModalFooter, FormGroup, Label, Input, Button, Form } from 'reactstrap';
import { addSip } from './modalSlice';
import { useDispatch } from 'react-redux';
import { setUpJsSip } from '../../util/jsSip';

function ModelSetting(props) {
  const disPatch = useDispatch();
  const { modal, toggleModel } = props;
  const [sipUri, setSipUri] = useState('');
  const [password, setPassword] = useState('');
  const [webSocket, setWebSocket] = useState('');

  const setSipToLocalStorage = (sip) => {
    localStorage.setItem('sip', JSON.stringify(sip));
  };
  const getSipToLocalStorage = () => {
    const sip = JSON.parse(localStorage.getItem('sip'));
    console.log(sip);
    if (!sip) return;
    setSipToLocalStorage(sip);
    setUpJsSip(sip);
    const action = addSip(sip);
    disPatch(action);
    const { sipUri, webSocket, password } = sip;
    setSipUri(sipUri);
    setPassword(password);
    setWebSocket(webSocket);
  };

  const handleClickSubmit = (e) => {
    e.preventDefault();
    toggleModel();
    const sip = { sipUri, webSocket, password };
    setSipToLocalStorage(sip);
    setUpJsSip(sip);
    const action = addSip(sip);
    disPatch(action);
  };

  const handleShowModal = () => {
    if (!modal) return;
    else return toggleModel();
  };

  useEffect(() => {
    getSipToLocalStorage();
  }, []);

  return (
    <>
      <Modal isOpen={modal} toggle={handleShowModal}>
        <Form onSubmit={handleClickSubmit}>
          <ModalHeader>adaad</ModalHeader>
          <ModalBody>
            <FormGroup>
              <Label for="sipUrl">SIP URI</Label>
              <Input
                type="text"
                name="sipUrl"
                id="sipUrl"
                placeholder="sip@sip.vn:port"
                value={sipUri}
                onChange={(e) => setSipUri(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="password">Password</Label>
              <Input
                type="password"
                name="password"
                id="password"
                placeholder="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </FormGroup>
            <FormGroup>
              <Label for="webSocket">WebSocket URI</Label>
              <Input
                type="text"
                name="webSocket"
                id="webSocket"
                placeholder="wss://myhost.com:port"
                value={webSocket}
                onChange={(e) => setWebSocket(e.target.value)}
              />
            </FormGroup>
          </ModalBody>
          <ModalFooter>
            <Button onClick={toggleModel}>Cancel</Button>
            <Button color="success" type="submit">
              OK
            </Button>
          </ModalFooter>
        </Form>
      </Modal>
    </>
  );
}

export default ModelSetting;
