import { createSlice } from '@reduxjs/toolkit';
const sip = createSlice({
  name: 'sip',
  initialState: {},
  reducers: {
    addSip: (state, action) => {
      return action.payload;
    },
  },
});

const { reducer, actions } = sip;
export const { addSip } = actions;
export default reducer;
