import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Navbar from './components/Navbar/Navbar';
import Call from './features/call/Call';
import './App.css';

function App() {
  return (
    <div style={{ width: '100%', height: '100vh' }}>
      <BrowserRouter>
        <Navbar />
        <div className="content">
          <Routes>
            <Route path="/" element={<Call />} />
          </Routes>
        </div>
      </BrowserRouter>
    </div>
  );
}

export default App;
